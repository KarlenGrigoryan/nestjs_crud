import {
  Body,
  Controller,
  HttpStatus,
  Get,
  Post,
  Put,
  Delete,
  Res,
  Param,
  UseInterceptors,
  UseGuards,
  CacheInterceptor,
  CacheKey,
  CACHE_MANAGER,
  Inject,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { Response } from 'express';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as _ from 'lodash';

//
import { UserService } from './user.service';
import { Roles } from '../../decorators/roles.decorator';

// dto
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDto } from './dto/user.dto';
// guards
import { RolesGuard } from '../../guards/roles.guard';
import { JwtAuthGuard } from '../../guards/jwt-auth.guard';
import { User } from './entities/user.entity';

@Controller('user')
@ApiTags('user')
@UseInterceptors(CacheInterceptor)
@UseInterceptors(ClassSerializerInterceptor)
export class UserController {
  constructor(private readonly usersService: UserService, @Inject(CACHE_MANAGER) private readonly cacheManager) {}

  @Get('/all')
  @ApiBearerAuth()
  @ApiResponse({ status: 200, type: UserDto, isArray: true })
  @Roles({database: 'users', role: 'read'})
  // @UseGuards(JwtAuthGuard, RolesGuard)
  @CacheKey('users')
  async getUsers(): Promise<UserDto[]> {
    return this.usersService.findAll()
  }

  @Get(':id')
  @ApiResponse({ status: 200, type: UserDto })
  @UseGuards(JwtAuthGuard)
  async getUser(@Param('id') id: string) {
    return this.usersService.findOne(id)
  }

  @Put(':id')
  @ApiBearerAuth()
  @ApiResponse({ status: 200, type: UserDto })
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles({database: 'users', role: 'readUpdate'})
  async updateUser(@Param('id') id: string, @Body() user: UpdateUserDto): Promise<User> {
    const updatedUser: User = await this.usersService.update(id, user);
    this.updateCache(id, updatedUser);

    return updatedUser
  }

  @Delete(':id')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles({database: 'users', role: 'readDelete'})
  async deleteUser(@Param('id') id: string) {
    const response = await this.usersService.deleteOne(id);
    this.updateCache(id);
    return response
  }

  @Post()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles( {database: 'users', role: 'readWrite'})
  @ApiResponse({ status: 201, description: 'The user has been successfully created.', type: UserDto })
  async createUser(@Body() createUserDto: CreateUserDto, @Res() res: Response) {
    const newUser = await this.usersService.create(createUserDto);
    this.insertInCache(newUser);
    res.status(HttpStatus.CREATED).json(newUser);
  }

  private async updateCache(id: string | number, updatedUser?: User) {
    const cachedUsers: User[] = await this.cacheManager.get('users');
    if (!cachedUsers) {
      return
    }
    const index = _.findIndex(cachedUsers, { id });
    if (updatedUser) {
      if (index > -1) {
        cachedUsers.splice(index, 1);
      } else {
        cachedUsers.push(updatedUser)
      }
      this.cacheManager.set('users', cachedUsers);
      return
    }
    if (index > -1) {
      cachedUsers.splice(index, 1);
    }
    this.cacheManager.set('users', cachedUsers);
  }

  private async insertInCache(newUser: User) {
    const cachedUsers: User[] = await this.cacheManager.get('users');
    if (!cachedUsers) {
      return
    }
    cachedUsers.push(newUser);
    this.cacheManager.set('users', cachedUsers);
  }
}
