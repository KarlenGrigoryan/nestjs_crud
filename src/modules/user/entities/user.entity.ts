import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Exclude } from 'class-transformer';

import { IRole } from '../../../interfaces/role.interface';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
    id: number;

  @Column({ unique: true })
  name: string;

  @Column({ unique: true })
  email: string;

  @Column({ type: 'jsonb', default: [] })
  @Exclude()
  roles: IRole[];

  @Column({
    name: 'password',
    length: 70
  })
  @Exclude()
  password: string;
  
  @CreateDateColumn({ name: 'created_at' })
  readonly createdAt?: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  readonly updatedAt?: Date;
}
