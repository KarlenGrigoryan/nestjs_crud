import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import * as bcrypt from 'bcrypt';

// dto
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

// entities
import { User } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private readonly userModel: Repository<User>) {}

  async findAll(): Promise<User[]> {
    let users: User[];
    try {
      users = await this.userModel.find();
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return users
  }

  async findOne(id: string | number): Promise<User> {
    let user: User;
    try {
      user = await this.userModel.findOne(
        {
          where: { id }
        }
      );
    }catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return user
  }

  async findOneByUsername(username: string): Promise<User> {
    let user: User;
    try {
      user = await this.userModel.findOne({ where: { name: username } })
    }catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return user
  }

  async update(id, newData: UpdateUserDto): Promise<User> {
    let user: User;
    try {
      const updateResult: UpdateResult = await this.userModel.update(id, newData);
      const success: boolean = updateResult.affected > 0;

      if (!success) {
        throw new HttpException('Failed to update user', HttpStatus.BAD_REQUEST);
      }
      user = await this.userModel.findOne(id);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return user
  }

  async deleteOne(id) {
    try {
      const deleteResult: DeleteResult = await this.userModel.delete(id);
      const success: boolean = deleteResult.affected > 0;

      if (!success) {
        throw new HttpException('Failed to delete user', HttpStatus.BAD_REQUEST);
      }

      return { message: 'user is successfully deleted' }
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }
  }

  async create(user: CreateUserDto): Promise<User> {
    let response: User;
    // save user
    try {
      const saltRounds = 10;
      user.password = bcrypt.hashSync(user.password, saltRounds);
      response = await this.userModel.save(user)
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return response
  }
}
