import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Work } from './entities/work.entity';
import { WorkController } from './work.controller';
import { WorkService } from './work.service';
import { EmployeeModule } from '../employee/employee.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Work]),
    EmployeeModule
  ],
  controllers: [WorkController],
  providers: [WorkService]
})
export class WorkModule {}
