import { Body, ClassSerializerInterceptor, Controller, Get, Param, Post, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { CreateWorkDto } from './dto/create-work.dto';
import { WorkService } from './work.service';

@Controller('work')
@ApiTags('work')
@UseInterceptors(ClassSerializerInterceptor)
export class WorkController {
  constructor(private readonly workService: WorkService) {}

  @Get()
  async getWorks() {
    return this.workService.findAll()
  }

  @Get(':id')
  async getWork(@Param('id') id: string) {
    return this.workService.findOne(id)
  }

  @Post()
  async createWork(@Body() work: CreateWorkDto) {
    return this.workService.createWork(work)
  }

}
