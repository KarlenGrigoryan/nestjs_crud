import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm'
import * as _ from 'lodash';

import { Work } from './entities/work.entity';
import { CreateWorkDto } from './dto/create-work.dto';
import { Employee } from '../employee/entities/employee.entity';
import { EmployeeService } from '../employee/employee.service';

@Injectable()
export class WorkService {
  constructor(@InjectRepository(Work) private readonly workModel: Repository<Work>, private readonly employeeService: EmployeeService) {}

  public async findAll(): Promise<Work[]> {
    let works: Work[];

    try{
      works = await this.workModel.find({ relations: ['employee']});
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return works
  }

  public async findOne(id: string): Promise<Work> {
    let work: Work;

    try {
      work = await this.workModel
        .findOne(
          {
            where: { id },
            relations: ['employee']
            },
        );

      if (!work) {
        throw new HttpException('Work was not found', HttpStatus.NOT_FOUND)
      }
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return work
  }

  public async createWork(work: CreateWorkDto): Promise<Work> {
    let createdWork: Work;
    const workClone: Work = Object.create(work);

    try {
      const employeeId = _.get(work, 'employeeId', undefined);
      if (employeeId) {
        const employee: Employee = await this.employeeService.findOne(employeeId);
        workClone.employee = employee
      }
      createdWork = await this.workModel.save(workClone)
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return createdWork
  }
}
