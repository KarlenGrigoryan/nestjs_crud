import {
  Body,
  Controller,
  Post,
  Req,
  Headers,
  Request,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';

// services
import { AuthService } from './auth.service';

// dto
import { LoginDto } from './dto/login.dto';
import { LoginResponseDto } from './dto/login-response.dto';
import { RegistrationDto } from './dto/registration.dto';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService
  ) {}

  @Post('/login')
  @ApiResponse({ status: 200, description: 'The user has been successfully login.', type: LoginResponseDto})
  async login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }

  @Post('/logout')
  @ApiBearerAuth()
  logout(@Req() request: Request, @Headers('authorization') authorization: string ) {
    return this.authService.logout(authorization)
  }

  // @Post('/employee/login')
  // @UseGuards(AuthGuard('local'))

  @Post('/registration')
  async registration(@Body() params: RegistrationDto) {
    return this.authService.registration(params)
  }

}
