import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { catchError, map } from 'rxjs/operators';

import { UserService } from '../user/user.service';
import { LoginResponseDto } from './dto/login-response.dto';

@Injectable()
export class AuthService implements OnApplicationBootstrap {
  constructor(
    private readonly usersService: UserService,
    @Inject('AUTH_S') private readonly authMicroservice: ClientProxy,
  ) {}

  async onApplicationBootstrap() {
    await this.authMicroservice.connect();
  }

  registration(newUser) {
    const pattern = { cmd: 'auth.registration' };

    return this.authMicroservice.send(pattern, newUser).pipe(
      map(response => response),
      catchError(err => {
        throw new HttpException(err, HttpStatus.BAD_REQUEST);
      }),
    );
  }

  login(user) {
    const pattern = { cmd: 'auth.login' };

    return this.authMicroservice.send<LoginResponseDto>(pattern, user).pipe(
      map((response: LoginResponseDto) => response),
      catchError(err => {
        throw new HttpException(err, HttpStatus.BAD_REQUEST);
      }),
    );
  }

  logout(authorization: string) {
    const pattern = { cmd: 'auth.logout' };

    return this.authMicroservice.send<string>(pattern, authorization).pipe(
      map(response => response),
      catchError(err => {
        throw new HttpException(err, HttpStatus.BAD_REQUEST);
      }),
    );
  }
}
