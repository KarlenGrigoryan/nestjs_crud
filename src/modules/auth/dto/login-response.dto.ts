import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

export class LoginResponseDto {
  @ApiProperty()
  access_token?: string;

  @Exclude()
  roles: string;
}