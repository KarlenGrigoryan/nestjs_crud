import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { Employee } from './entities/employee.entity';
import { CreateEmployeeDto } from './dto/create-employee.dto';

@Injectable()
export class EmployeeService {
  constructor(@InjectRepository(Employee) private readonly employeeModel: Repository<Employee>) {}

  public async findAll(): Promise<Employee[]> {
    let employees: Employee[];

    try{
      employees = await this.employeeModel.find({ relations: ['works']});
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return employees
  }

  public async findOne(id: string | number): Promise<Employee> {
    let employee: Employee;

    try {
      employee = await this.employeeModel
        .findOne(
          {
            where: { id },
            relations: ['works']
          }
        );

      if (!employee) {
        throw new HttpException('Employee was not found', HttpStatus.NOT_FOUND)
      }
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return employee
  }

  public async create(employee: CreateEmployeeDto): Promise<Employee> {
    let createdEmployee: Employee;

    try {
      const saltRounds = 10;
      const employeeClone = Object.create(employee);
      employeeClone.password = bcrypt.hashSync(employee.password, saltRounds);
      createdEmployee = await this.employeeModel.save(employeeClone);
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }

    return createdEmployee
  }

  public async deleteOne(id: string) {
    try {
      const deleteResult: DeleteResult = await this.employeeModel.delete(id);
      const success: boolean = deleteResult.affected > 0;

      if (!success) {
        throw new HttpException('Failed to delete employee', HttpStatus.BAD_REQUEST);
      }

      return { message: 'The employee is successfully deleted with him/her works' }
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST)
    }
  }
}
