import { IsString, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { IRole } from '../../../interfaces/role.interface';

export class CreateEmployeeDto {
  @ApiProperty()
  @IsString()
  readonly firstName: string;

  @ApiProperty()
  @IsString()
  readonly lastName: string;

  @ApiProperty()
  @IsEmail()
  readonly email: string;

  @ApiProperty()
  @IsString()
  readonly password: string;

  @ApiProperty({ required: false })
  readonly roles?: IRole[]
}