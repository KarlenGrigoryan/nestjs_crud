import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Exclude } from 'class-transformer';

import { Work } from '../../work/entities/work.entity';
import { IRole } from '../../../interfaces/role.interface';

@Entity('employees')
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column({ type: 'jsonb', default: [], nullable: true })
  @Exclude()
  roles: IRole[];

  @Column({nullable: true})
  @Exclude()
  password: string;

  @OneToMany(type => Work, work => work.employee, { cascade: true, onDelete: 'CASCADE' })
  works: Work[];

  @CreateDateColumn({ name: 'created_at' })
  readonly createdAt?: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  readonly updatedAt?: Date;
}
