import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { EmployeeService } from './employee.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';

@Controller('employee')
@ApiTags('employee')
@UseInterceptors(ClassSerializerInterceptor)
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) {}

  @Get()
  async getEmployees() {
    return this.employeeService.findAll()
  }

  @Get(':id')
  async getEmployee(@Param('id') id: string) {
    return this.employeeService.findOne(id)
  }

  @Post()
  async createEmployee(@Body() employee: CreateEmployeeDto) {
    return this.employeeService.create(employee);
  }

  @Delete(':id')
  async deleteEmployee(@Param('id') id: string) {
    return this.employeeService.deleteOne(id)
  }
}
