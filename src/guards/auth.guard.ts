import {
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpException,
  OnApplicationBootstrap,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate, OnApplicationBootstrap {
  private client: ClientProxy;

  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: "127.0.0.1",
        port: 8888,
      },
    });
  }

  async onApplicationBootstrap() {
    await this.client.connect();
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const { headers: {authorization} } = request;

    return this.client.send<boolean>({cmd: 'auth.verify'}, authorization).pipe(
      map((isVerify: boolean) => isVerify),
      catchError(err => {
        throw new HttpException(err, err.statusCode)
      })
    );
  }
}