import { Injectable, CanActivate, ExecutionContext, HttpException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { IRole } from '../interfaces/role.interface';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class RolesGuard implements CanActivate {
  private client: ClientProxy;

  constructor(private reflector: Reflector) {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: "127.0.0.1",
        port: 8888,
      },
    });
  }

  canActivate(context: ExecutionContext): boolean | Observable<boolean> {
    const role = this.reflector.get<IRole>('role', context.getHandler());
    if (!role) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const { headers: {authorization} } = request;
    return this.client.send<boolean>({cmd: 'auth.roles'}, {role, authorization: authorization}).pipe(
      map((isMatch: boolean) => isMatch),
      catchError(err => {
        throw new HttpException(err, err.statusCode)
      })
    );
  }
}
