import * as bcrypt from 'bcrypt';

const saltRounds = 10;

export const AdminSeed = {
  name: 'Admin',
  email: 'admin@gmail.com',
  password: bcrypt.hashSync("pass", saltRounds),
  roles: [
    { database: 'users', role: 'read' },
    { database: 'users', role: 'readWrite' },
    { database: 'users', role: 'readUpdate' },
    { database: 'users', role: 'readDelete' },
  ]
};
