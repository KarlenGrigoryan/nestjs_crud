import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateEmployeesTable1584966355157 implements MigrationInterface {
    name = 'CreateEmployeesTable1584966355157';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'employees',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment',
                },
                {
                    name: 'firstName',
                    type: 'varchar',
                    length: '20'
                },
                {
                    name: 'lastName',
                    type: 'varchar',
                    length: '20'
                },
                {
                    name: 'email',
                    type: 'varchar',
                    isUnique: true
                },
                {
                    name: 'roles',
                    type: 'jsonb'
                },
                {
                    name: 'password',
                    type: 'varchar',
                    length: '70'
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()',
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()',
                },
            ],
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable('employees');
    }

}
