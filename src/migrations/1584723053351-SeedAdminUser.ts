import { MigrationInterface, QueryRunner, getRepository } from "typeorm";

import { AdminSeed } from '../seed/admin.seed';

export class SeedAdminUser1584719905554 implements MigrationInterface {
    name = 'SeedAdminUser1584719905554';

    public async up(queryRunner: QueryRunner): Promise<any> {
        await getRepository('users').save(AdminSeed)
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await getRepository('users').delete(AdminSeed)
    }

}
