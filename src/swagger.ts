import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { OpenAPIObject } from '@nestjs/swagger/dist/interfaces';
import { Logger } from '@nestjs/common';

// modules
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { WorkModule } from './modules/work/work.module';
import { EmployeeModule } from './modules/employee/employee.module';

export default class Swagger {
  private readonly app;
  private readonly logger: Logger;

  constructor(app) {
    this.app = app;
    this.logger = new Logger();
    this.createMainDocument();
    this.logger.log('Swagger Api documentation available under "/api-docs" path')
  }

  private createMainDocument() {
    const options = {title: 'API example', desc: 'The API description', version: '1.0'};
    const config = this.generateConfig(options, true);
    const document: OpenAPIObject = this.createDocument(
      config,
      [
        AuthModule,
        UserModule,
        WorkModule,
        EmployeeModule
      ]
    );
    this.setupSwagger('/api-docs', document);
    return this
  }

  public createUserDocument() {
    const options = {title: 'User example', desc: 'The user API description', version: '1.0'};
    const config = this.generateConfig(options, true);
    const document: OpenAPIObject = this.createDocument(config, [UserModule]);
    this.setupSwagger('api-docs/user', document);
    return this
  }

  public createAuthDocument() {
    const options = {title: 'Auth example', desc: 'The Auth API description', version: '1.0'};
    const config = this.generateConfig(options, true);
    const document: OpenAPIObject = this.createDocument(config, [AuthModule]);
    this.setupSwagger('api-docs/auth', document);
    return this
  }

  public createWorkDocument() {
    const options = {title: 'Work example', desc: 'The work API description', version: '1.0'};
    const config = this.generateConfig(options, true);
    const document: OpenAPIObject = this.createDocument(config, [WorkModule]);
    this.setupSwagger('api-docs/work', document);
    return this
  }

  public createEmployeeDocument() {
    const options = {title: 'Employee example', desc: 'The employee API description', version: '1.0'};
    const config = this.generateConfig(options, true);
    const document: OpenAPIObject = this.createDocument(config, [EmployeeModule]);
    this.setupSwagger('api-docs/employee', document);
    return this
  }

  private generateConfig({ title, desc, version }, isAuth = false) {
    if (isAuth) {
      return new DocumentBuilder()
        .setTitle(title)
        .setDescription(desc)
        .setVersion(version)
        .addBearerAuth()
        .build();
    }
    return new DocumentBuilder()
      .setTitle(title)
      .setDescription(desc)
      .setVersion(version)
      .build();
  }

  private createDocument(config, modules: any[]): OpenAPIObject {
    return SwaggerModule.createDocument(this.app, config, {
      include: [...modules]
    })
  }

  private setupSwagger(path: string, document: OpenAPIObject) {
    SwaggerModule.setup(path, this.app, document);
  }
}